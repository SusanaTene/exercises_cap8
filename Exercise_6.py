#-.-.-.-.-.-.-UNIVERSIDAD NACIONAL DE LOJA-.-.-.-.-.-.-.-
#Autora: Lilia Susana Tene Medina.  Email: lilia.tene@unl.edu.ec

#Ejercicio 6: Reescribe el programa que pide al usuario una lista de
#números e imprime el máximo y el mínimo de los números al final cuando
#el usuario ingresa “hecho”. Escribe el programa para almacenar los
#números que el usuario ingrese en una lista, y utiliza las funciones max()
#y min() para calcular el máximo y el mínimo después de que el bucle termine.

def main():
    print("Maximo y Minimo")
    valor = []
    while 1:
        num = input(f"Ingresa un número: ")
        if num!='hecho':
            valor.append(num)
        else:
            break
    print(max(valor))
    print(min(valor))

if __name__ == "__main__":
    main()