#-.-.-.-.-.-.-UNIVERSIDAD NACIONAL DE LOJA-.-.-.-.-.-.-.-
#Autora: Lilia Susana Tene Medina.  Email: lilia.tene@unl.edu.ec

#Ejercicio 5: Escribir un programa para leer a través de datos de una bandeja de
# entrada de correo y cuando encuentres una línea que comience
#con “From”, dividir la línea en palabras utilizando la función split.
#Estamos interesados en quién envió el mensaje, lo cual es la segunda
#palabra en las líneas que comienzan con From

manejador = open('mbox-short.txt')
contador = 0
for linea in manejador:
    palabras = linea.split()
    # print 'Depuración:', palabras
    if len(palabras) == 0 : continue
    if palabras[0] != 'From' : continue
    print(palabras[2])
